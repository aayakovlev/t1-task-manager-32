package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public interface UserEndpoint {

    @NotNull
    UserChangePasswordResponse changePassword(
            @NotNull final UserChangePasswordRequest request
    ) throws AbstractException;

    @NotNull
    UserLockResponse lock(
            @NotNull final UserLockRequest request
    ) throws AbstractException;

    @NotNull
    UserRegistryResponse registry(
            @NotNull final UserRegistryRequest request
    ) throws AbstractException;

    @NotNull
    UserRemoveResponse remove(
            @NotNull final UserRemoveRequest request
    ) throws AbstractException;

    @NotNull
    UserShowAllResponse showAll(
            @NotNull final UserShowAllRequest request
    ) throws AbstractException;

    @NotNull
    UserSetAllResponse setAll(
            @NotNull final UserSetAllRequest request
    ) throws AbstractException;

    @NotNull
    UserUnlockResponse unlock(
            @NotNull final UserUnlockRequest request
    ) throws AbstractException;

    @NotNull
    UserUpdateProfileResponse update(
            @NotNull final UserUpdateProfileRequest request
    ) throws AbstractException;

}
