package ru.t1.aayakovlev.tm.endpoint.impl;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.endpoint.DomainEndpoint;
import ru.t1.aayakovlev.tm.service.DomainService;
import ru.t1.aayakovlev.tm.service.ServiceLocator;

public final class DomainEndpointImpl extends AbstractEndpoint implements DomainEndpoint {

    public DomainEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private DomainService getDomainService() {
        return getServiceLocator().getDomainService();
    }

    @NotNull
    @Override
    public DataBackupLoadResponse backupLoad(@NotNull final DataBackupLoadRequest request) {
        getDomainService().backupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    public DataBackupSaveResponse backupSave(@NotNull final DataBackupSaveRequest request) {
        getDomainService().backupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    public DataBase64LoadResponse base64Load(@NotNull final DataBase64LoadRequest request) {
        getDomainService().base64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse base64Save(@NotNull final DataBase64SaveRequest request) {
        getDomainService().base64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse binaryLoad(@NotNull final DataBinaryLoadRequest request) {
        getDomainService().binaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse binarySave(@NotNull final DataBinarySaveRequest request) {
        getDomainService().binarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXmlResponse jsonLoadFXml(@NotNull final DataJsonLoadFasterXmlRequest request) {
        getDomainService().jsonLoadFXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadJaxBResponse jsonLoadJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        getDomainService().jsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXmlResponse jsonSaveFXml(@NotNull final DataJsonSaveFasterXmlRequest request) {
        getDomainService().jsonSaveFXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveJaxBResponse jsonSaveJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        getDomainService().jsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadFasterXmlResponse xmlLoadFXml(@NotNull final DataXmlLoadFasterXmlRequest request) {
        getDomainService().xmlLoadFXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadJaxBResponse xmlLoadJaxB(@NotNull final DataXmlLoadJaxBRequest request) {
        getDomainService().xmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXmlResponse xmlSaveFXml(@NotNull final DataXmlSaveFasterXmlRequest request) {
        getDomainService().xmlSaveFXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveJaxBResponse xmlSaveJaxB(@NotNull final DataXmlSaveJaxBRequest request) {
        getDomainService().xmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXmlResponse yamlLoad(@NotNull final DataYamlLoadFasterXmlRequest request) {
        getDomainService().yamlLoad();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXmlResponse yamlSave(@NotNull final DataYamlSaveFasterXmlRequest request) {
        getDomainService().yamlSave();
        return new DataYamlSaveFasterXmlResponse();
    }

}
