package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public interface ProjectEndpoint {

    @NotNull
    ProjectChangeStatusByIdResponse changeStatusById(
            @NotNull final ProjectChangeStatusByIdRequest request
    ) throws AbstractException;

    @NotNull
    ProjectChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) throws AbstractException;

    @NotNull
    ProjectClearResponse clear(
            @NotNull final ProjectClearRequest request
    ) throws AbstractException;

    @NotNull
    ProjectCompeteByIdResponse completeById(
            @NotNull final ProjectCompeteByIdRequest request
    ) throws AbstractException;

    @NotNull
    ProjectCompeteByIdResponse completeByIndex(
            @NotNull final ProjectCompleteByIndexRequest request
    ) throws AbstractException;

    @NotNull
    ProjectCreateResponse create(
            @NotNull final ProjectCreateRequest request
    ) throws AbstractException;

    @NotNull
    ProjectRemoveByIdResponse removeById(
            @NotNull final ProjectRemoveByIdRequest request
    ) throws AbstractException;

    @NotNull
    ProjectRemoveByIndexResponse removeByIndex(
            @NotNull final ProjectRemoveByIndexRequest request
    ) throws AbstractException;

    @NotNull
    ProjectSetAllResponse setAll(
            @NotNull final ProjectSetAllRequest request
    ) throws AbstractException;

    @NotNull
    ProjectShowAllResponse showAll(
            @NotNull final ProjectShowAllRequest request
    ) throws AbstractException;

    @NotNull
    ProjectShowByIdResponse showById(
            @NotNull final ProjectShowByIdRequest request
    ) throws AbstractException;

    @NotNull
    ProjectShowByIndexResponse showByIndex(
            @NotNull final ProjectShowByIndexRequest request
    ) throws AbstractException;

    @NotNull
    ProjectStartByIdResponse startById(
            @NotNull final ProjectStartByIdRequest request
    ) throws AbstractException;

    @NotNull
    ProjectStartByIndexResponse startByIndex(
            @NotNull final ProjectStartByIndexRequest request
    ) throws AbstractException;

    @NotNull
    ProjectUpdateByIdResponse updateById(
            @NotNull final ProjectUpdateByIdRequest request
    ) throws AbstractException;

    @NotNull
    ProjectUpdateByIndexResponse updateByIndex(
            @NotNull final ProjectUpdateByIndexRequest request
    ) throws AbstractException;

}
