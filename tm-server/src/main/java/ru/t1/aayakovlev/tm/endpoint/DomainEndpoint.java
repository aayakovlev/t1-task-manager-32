package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;

public interface DomainEndpoint {

    @NotNull
    DataBackupLoadResponse backupLoad(@NotNull final DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse backupSave(@NotNull final DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse base64Load(@NotNull final DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse base64Save(@NotNull final DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse binaryLoad(@NotNull final DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse binarySave(@NotNull final DataBinarySaveRequest request);

    @NotNull
    DataJsonLoadFasterXmlResponse jsonLoadFXml(@NotNull final DataJsonLoadFasterXmlRequest request);

    @NotNull
    DataJsonLoadJaxBResponse jsonLoadJaxB(@NotNull final DataJsonLoadJaxBRequest request);

    @NotNull
    DataJsonSaveFasterXmlResponse jsonSaveFXml(@NotNull final DataJsonSaveFasterXmlRequest request);

    @NotNull
    DataJsonSaveJaxBResponse jsonSaveJaxB(@NotNull final DataJsonSaveJaxBRequest request);

    @NotNull
    DataXmlLoadFasterXmlResponse xmlLoadFXml(@NotNull final DataXmlLoadFasterXmlRequest request);

    @NotNull
    DataXmlLoadJaxBResponse xmlLoadJaxB(@NotNull final DataXmlLoadJaxBRequest request);

    @NotNull
    DataXmlSaveFasterXmlResponse xmlSaveFXml(@NotNull final DataXmlSaveFasterXmlRequest request);

    @NotNull
    DataXmlSaveJaxBResponse xmlSaveJaxB(@NotNull final DataXmlSaveJaxBRequest request);

    @NotNull
    DataYamlLoadFasterXmlResponse yamlLoad(@NotNull final DataYamlLoadFasterXmlRequest request);

    @NotNull
    DataYamlSaveFasterXmlResponse yamlSave(@NotNull final DataYamlSaveFasterXmlRequest request);

}
