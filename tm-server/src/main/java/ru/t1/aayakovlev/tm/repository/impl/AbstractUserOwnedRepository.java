package ru.t1.aayakovlev.tm.repository.impl;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;
import ru.t1.aayakovlev.tm.repository.UserOwnedRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractBaseRepository<M>
        implements UserOwnedRepository<M> {

    @Override
    public void clear(@NotNull final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public int count(@NotNull final String userId) {
        return findAll(userId).size();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws AbstractFieldException {
        return findById(userId, id) != null;
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        return models.stream()
                .filter((m) -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    @Nullable
    public M findById(@NotNull final String userId, @NotNull final String id) throws AbstractFieldException {
        return models.stream()
                .filter((m) -> userId.equals(m.getUserId()))
                .filter((m) -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @NotNull
    public M findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    @NotNull
    public M remove(@NotNull final String userId, @NotNull final M model) throws AbstractException {
        return removeById(userId, model.getId());
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    @NotNull
    public M removeById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        @Nullable final M model = findById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Override
    @NotNull
    public M removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final M model = findByIndex(userId, index);
        return remove(model);
    }

    @Override
    @NotNull
    public M save(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        return save(model);
    }

}
