package ru.t1.aayakovlev.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.endpoint.*;
import ru.t1.aayakovlev.tm.endpoint.impl.*;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.UserRepository;
import ru.t1.aayakovlev.tm.repository.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.UserRepositoryImpl;
import ru.t1.aayakovlev.tm.service.*;
import ru.t1.aayakovlev.tm.service.impl.*;
import ru.t1.aayakovlev.tm.util.SystemUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.PID_FILENAME;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Backup backup = new Backup(this);

    @Getter
    @NotNull
    private final LoggerService loggerService = new LoggerServiceImpl();

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepositoryImpl();

    @Getter
    @NotNull
    private final ProjectService projectService = new ProjectServiceImpl(projectRepository);

    @NotNull
    private final TaskRepository taskRepository = new TaskRepositoryImpl();

    @Getter
    @NotNull
    private final TaskService taskService = new TaskServiceImpl(taskRepository);

    @Getter
    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskServiceImpl(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final SystemEndpoint systemEndpoint = new SystemEndpointImpl(this);

    @NotNull
    private final DomainEndpoint domainEndpoint = new DomainEndpointImpl(this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointImpl(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointImpl(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointImpl(this);

    @NotNull
    private final UserRepository userRepository = new UserRepositoryImpl();

    @Getter
    @NotNull
    private final UserService userService = new UserServiceImpl(
            propertyService,
            userRepository,
            projectRepository,
            taskRepository
    );

    @Getter
    @NotNull
    private final AuthService authService = new AuthServiceImpl(propertyService, userService);

    @Getter
    @NotNull
    private final DomainService domainService = new DomainServiceImpl(
            authService,
            projectService,
            taskService,
            userService
    );

    {
        server.registry(DataBackupLoadRequest.class, domainEndpoint::backupLoad);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::backupSave);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::base64Load);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::base64Save);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::binaryLoad);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::binarySave);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::jsonLoadFXml);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::jsonLoadJaxB);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::jsonSaveFXml);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::jsonSaveJaxB);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::xmlLoadFXml);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::xmlLoadJaxB);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::xmlSaveFXml);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::xmlSaveJaxB);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::yamlLoad);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::yamlSave);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clear);
        server.registry(ProjectCompeteByIdRequest.class, projectEndpoint::completeById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::create);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeByIndex);
        server.registry(ProjectShowAllRequest.class, projectEndpoint::showAll);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateByIndex);

        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clear);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::create);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeByIndex);
        server.registry(TaskShowAllRequest.class, taskEndpoint::showAll);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showByIndex);
        server.registry(TaskShowByProjectIdRequest.class, taskEndpoint::showByProjectId);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindFromProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateByIndex);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateByIndex);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changePassword);
        server.registry(UserLockRequest.class, userEndpoint::lock);
        server.registry(UserRegistryRequest.class, userEndpoint::registry);
        server.registry(UserRemoveRequest.class, userEndpoint::remove);
        server.registry(UserUnlockRequest.class, userEndpoint::unlock);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::update);
    }

    private void initAdmin() throws AbstractException {
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void initBackup() {
        backup.init();
    }

    private void initServer() {
        server.init();
    }

    private void initPID() {
        @NotNull final String filename = PID_FILENAME;
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
        } catch (@NotNull final IOException e) {
            e.printStackTrace();
        }
    }

    public void start() throws AbstractException {
        initPID();
        initAdmin();
        initBackup();
        initServer();
        loggerService.info(
                "___________   _____      _________                                       \n" +
                        "\\__    ___/  /     \\    /   _____/  ____  _______ ___  __  ____  _______ \n" +
                        "  |    |    /  \\ /  \\   \\_____  \\ _/ __ \\ \\_  __ \\\\  \\/ /_/ __ \\ \\_  __ \\\n" +
                        "  |    |   /    Y    \\  /        \\\\  ___/  |  | \\/ \\   / \\  ___/  |  | \\/\n" +
                        "  |____|   \\____|__  / /_______  / \\___  > |__|     \\_/   \\___  > |__|   \n" +
                        "                   \\/          \\/      \\/                     \\/         ");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    private void stop() {
        server.stop();
        backup.stop();
        loggerService.info("*** APPLICATION SHUTTING DOWN ***");
    }

}
