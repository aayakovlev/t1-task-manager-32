package ru.t1.aayakovlev.tm.endpoint.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.endpoint.ProjectEndpoint;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.service.ProjectService;
import ru.t1.aayakovlev.tm.service.ProjectTaskService;
import ru.t1.aayakovlev.tm.service.ServiceLocator;

import java.util.Collections;
import java.util.List;

public final class ProjectEndpointImpl extends AbstractEndpoint implements ProjectEndpoint {

    public ProjectEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    private ProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeStatusById(
            @NotNull final ProjectChangeStatusByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectService().changeStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectService().changeStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectClearResponse clear(
            @NotNull final ProjectClearRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    public ProjectCompeteByIdResponse completeById(
            @NotNull final ProjectCompeteByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectService().changeStatusById(userId, id, Status.COMPLETED);
        return new ProjectCompeteByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectCompeteByIdResponse completeByIndex(
            @NotNull final ProjectCompleteByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectService().changeStatusByIndex(userId, index, Status.COMPLETED);
        return new ProjectCompeteByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectCreateResponse create(
            @NotNull final ProjectCreateRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeById(
            @NotNull final ProjectRemoveByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectTaskService().removeProjectById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeByIndex(
            @NotNull final ProjectRemoveByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectService().findByIndex(userId, index);
        getProjectTaskService().removeProjectById(userId, project.getId());
        return new ProjectRemoveByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectSetAllResponse setAll(
            @NotNull final ProjectSetAllRequest request
    ) throws AbstractException {
        check(request);
        if (request.getProjects() == null) request.setProjects(Collections.emptyList());
        @NotNull final List<Project> projects = (List<Project>) getProjectService().set(request.getProjects());
        return new ProjectSetAllResponse(projects);
    }

    @NotNull
    @Override
    public ProjectShowAllResponse showAll(
            @NotNull final ProjectShowAllRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Sort sort = request.getSort();
        @Nullable final String userId = request.getUserId();
        @Nullable final List<Project> projects = getProjectService().findAll(userId, sort);
        return new ProjectShowAllResponse(projects);
    }

    @NotNull
    @Override
    public ProjectShowByIdResponse showById(
            @NotNull final ProjectShowByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectService().findById(userId, id);
        return new ProjectShowByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectShowByIndexResponse showByIndex(
            @NotNull final ProjectShowByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectService().findByIndex(userId, index);
        return new ProjectShowByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectStartByIdResponse startById(
            @NotNull final ProjectStartByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectService().changeStatusById(userId, id, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectStartByIndexResponse startByIndex(
            @NotNull final ProjectStartByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new ProjectStartByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateById(
            @NotNull final ProjectUpdateByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse updateByIndex(
            @NotNull final ProjectUpdateByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @Nullable final Project project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

}
