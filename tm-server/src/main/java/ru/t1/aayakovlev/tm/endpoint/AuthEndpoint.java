package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.UserLoginRequest;
import ru.t1.aayakovlev.tm.dto.request.UserLogoutRequest;
import ru.t1.aayakovlev.tm.dto.request.UserShowProfileRequest;
import ru.t1.aayakovlev.tm.dto.response.UserLoginResponse;
import ru.t1.aayakovlev.tm.dto.response.UserLogoutResponse;
import ru.t1.aayakovlev.tm.dto.response.UserShowProfileResponse;

public interface AuthEndpoint {

    @NotNull
    UserLoginResponse login(@NotNull final UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull final UserLogoutRequest request);

    @NotNull
    UserShowProfileResponse profile(@NotNull final UserShowProfileRequest request);

}
