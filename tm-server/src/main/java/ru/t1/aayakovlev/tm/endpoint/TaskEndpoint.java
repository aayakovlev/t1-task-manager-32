package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public interface TaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindToProject(
            @NotNull final TaskBindToProjectRequest request
    ) throws AbstractException;

    @NotNull
    TaskChangeStatusByIdResponse changeStatusById(
            @NotNull final TaskChangeStatusByIdRequest request
    ) throws AbstractException;

    @NotNull
    TaskChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull final TaskChangeStatusByIndexRequest request
    ) throws AbstractException;

    @NotNull
    TaskClearResponse clear(
            @NotNull final TaskClearRequest request
    ) throws AbstractException;

    @NotNull
    TaskCompleteByIdResponse completeById(
            @NotNull final TaskCompleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    TaskCompleteByIndexResponse completeByIndex(
            @NotNull final TaskCompleteByIndexRequest request
    ) throws AbstractException;

    @NotNull
    TaskCreateResponse create(
            @NotNull final TaskCreateRequest request
    ) throws AbstractException;

    @NotNull
    TaskRemoveByIdResponse removeById(
            @NotNull final TaskRemoveByIdRequest request
    ) throws AbstractException;

    @NotNull
    TaskRemoveByIndexResponse removeByIndex(
            @NotNull final TaskRemoveByIndexRequest request
    ) throws AbstractException;

    @NotNull
    TaskSetAllResponse setAll(
            @NotNull final TaskSetAllRequest request
    ) throws AbstractException;

    @NotNull
    TaskShowAllResponse showAll(
            @NotNull final TaskShowAllRequest request
    ) throws AbstractException;

    @NotNull
    TaskShowByIdResponse showById(
            @NotNull final TaskShowByIdRequest request
    ) throws AbstractException;

    @NotNull
    TaskShowByIndexResponse showByIndex(
            @NotNull final TaskShowByIndexRequest request
    ) throws AbstractException;

    @NotNull
    TaskShowByProjectIdResponse showByProjectId(
            @NotNull final TaskShowByProjectIdRequest request
    ) throws AbstractException;

    @NotNull
    TaskStartByIdResponse startById(
            @NotNull final TaskStartByIdRequest request
    ) throws AbstractException;

    @NotNull
    TaskStartByIndexResponse startByIndex(
            @NotNull final TaskStartByIndexRequest request
    ) throws AbstractException;

    @NotNull
    TaskUnbindFromProjectResponse unbindFromProject(
            @NotNull final TaskUnbindFromProjectRequest request
    ) throws AbstractException;

    @NotNull
    TaskUpdateByIdResponse updateById(
            @NotNull final TaskUpdateByIdRequest request
    ) throws AbstractException;

    @NotNull
    TaskUpdateByIndexResponse updateByIndex(
            @NotNull final TaskUpdateByIndexRequest request
    ) throws AbstractException;

}
