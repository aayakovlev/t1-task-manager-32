package ru.t1.aayakovlev.tm.endpoint.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.endpoint.TaskEndpoint;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.service.ProjectTaskService;
import ru.t1.aayakovlev.tm.service.ServiceLocator;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.Collections;
import java.util.List;


public final class TaskEndpointImpl extends AbstractEndpoint implements TaskEndpoint {

    public TaskEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private TaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    ;

    @NotNull
    private ProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindToProject(
            @NotNull final TaskBindToProjectRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse(task);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeStatusById(
            @NotNull final TaskChangeStatusByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().changeStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull final TaskChangeStatusByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().changeStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskClearResponse clear(
            @NotNull final TaskClearRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse completeById(
            @NotNull final TaskCompleteByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().changeStatusById(userId, id, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse completeByIndex(
            @NotNull final TaskCompleteByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().changeStatusByIndex(userId, index, Status.COMPLETED);
        return new TaskCompleteByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskCreateResponse create(
            @NotNull final TaskCreateRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeById(
            @NotNull final TaskRemoveByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse removeByIndex(
            @NotNull final TaskRemoveByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskSetAllResponse setAll(
            @NotNull final TaskSetAllRequest request
    ) throws AbstractException {
        check(request);
        if (request.getTasks() == null) request.setTasks(Collections.emptyList());
        @NotNull final List<Task> tasks = (List<Task>) getTaskService().set(request.getTasks());
        return new TaskSetAllResponse(tasks);
    }

    @NotNull
    @Override
    public TaskShowAllResponse showAll(
            @NotNull final TaskShowAllRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Sort sort = request.getSort();
        @Nullable final String userId = request.getUserId();
        @Nullable final List<Task> tasks = getTaskService().findAll(userId, sort);
        return new TaskShowAllResponse(tasks);
    }

    @NotNull
    @Override
    public TaskShowByIdResponse showById(
            @NotNull final TaskShowByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().findById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskShowByIndexResponse showByIndex(
            @NotNull final TaskShowByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().findByIndex(userId, index);
        return new TaskShowByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskShowByProjectIdResponse showByProjectId(
            @NotNull final TaskShowByProjectIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = request.getUserId();
        @Nullable final List<Task> tasks = getTaskService().findByProjectId(userId, projectId);
        return new TaskShowByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse startById(
            @NotNull final TaskStartByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().changeStatusById(userId, id, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse startByIndex(
            @NotNull final TaskStartByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindFromProject(
            @NotNull final TaskUnbindFromProjectRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse(task);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateById(
            @NotNull final TaskUpdateByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse updateByIndex(
            @NotNull final TaskUpdateByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}
