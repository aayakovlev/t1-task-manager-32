package ru.t1.aayakovlev.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.AbstractRequest;
import ru.t1.aayakovlev.tm.dto.response.AbstractResponse;
import ru.t1.aayakovlev.tm.endpoint.Operation;

import java.util.LinkedHashMap;
import java.util.Map;

public final class Dispatcher {

    @NotNull
    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass,
            @NotNull final Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @NotNull
    @SneakyThrows
    @SuppressWarnings({"rawtypes", "unchecked"})
    public Object call(@NotNull final AbstractRequest request) {
        @NotNull final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
