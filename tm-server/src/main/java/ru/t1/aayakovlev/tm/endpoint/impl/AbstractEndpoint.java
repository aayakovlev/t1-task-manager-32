package ru.t1.aayakovlev.tm.endpoint.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.request.AbstractUserRequest;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AccessDeniedException;
import ru.t1.aayakovlev.tm.exception.auth.UserNotLoggedException;
import ru.t1.aayakovlev.tm.exception.entity.UserNotFoundException;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.service.ServiceLocator;
import ru.t1.aayakovlev.tm.service.UserService;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final ServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(@Nullable final AbstractUserRequest request) throws AbstractException {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
    }

    protected void check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) throws AbstractException {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        @NotNull final ServiceLocator serviceLocator = getServiceLocator();
        @NotNull final UserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new UserNotFoundException();
        @Nullable Role roleUser = user.getRole();
        final boolean check = roleUser == role;
        if (!check) throw new AccessDeniedException();
    }

}
