package ru.t1.aayakovlev.tm.endpoint.impl;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.ServerAboutRequest;
import ru.t1.aayakovlev.tm.dto.request.ServerVersionRequest;
import ru.t1.aayakovlev.tm.dto.response.ServerAboutResponse;
import ru.t1.aayakovlev.tm.dto.response.ServerVersionResponse;
import ru.t1.aayakovlev.tm.endpoint.SystemEndpoint;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.ServiceLocator;

public final class SystemEndpointImpl extends AbstractEndpoint implements SystemEndpoint {

    public SystemEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final PropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        @NotNull final PropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
