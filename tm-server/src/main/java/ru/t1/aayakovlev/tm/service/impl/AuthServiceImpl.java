package ru.t1.aayakovlev.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.FailedLoginException;
import ru.t1.aayakovlev.tm.exception.auth.PasswordIncorrectException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.exception.field.PasswordEmptyException;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.service.AuthService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.UserService;
import ru.t1.aayakovlev.tm.util.HashUtil;

public final class AuthServiceImpl implements AuthService {

    @NotNull
    private final PropertyService propertyService;

    @NotNull
    private final UserService userService;

    public AuthServiceImpl(@NotNull final PropertyService propertyService, @NotNull final UserService userService) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    public @NotNull User check(@Nullable String login, @Nullable String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new FailedLoginException();
        if (user.isLocked()) throw new FailedLoginException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new FailedLoginException();
        if (!hash.equals(user.getPasswordHash())) throw new PasswordIncorrectException();
        return user;
    }

    @Override
    @NotNull
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        return userService.create(login, password, email);
    }

}
