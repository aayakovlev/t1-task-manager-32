package ru.t1.aayakovlev.tm.endpoint.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.endpoint.UserEndpoint;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.service.ServiceLocator;
import ru.t1.aayakovlev.tm.service.UserService;

import java.util.Collections;
import java.util.List;

public final class UserEndpointImpl extends AbstractEndpoint implements UserEndpoint {

    public UserEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private UserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changePassword(
            @NotNull final UserChangePasswordRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String password = request.getPassword();
        @Nullable final String userId = request.getUserId();
        @Nullable final User user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    public UserLockResponse lock(
            @NotNull final UserLockRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final User user = getUserService().lockUserByLogin(login);
        return new UserLockResponse(user);
    }

    @NotNull
    @Override
    public UserRegistryResponse registry(
            @NotNull final UserRegistryRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @Nullable final User user = getUserService().create(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    public UserRemoveResponse remove(
            @NotNull final UserRemoveRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final User user = getUserService().removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @NotNull
    @Override
    public UserUnlockResponse unlock(
            @NotNull final UserUnlockRequest request
    ) throws AbstractException {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final User user = getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse(user);
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse update(
            @NotNull final UserUpdateProfileRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final String userId = request.getUserId();
        @Nullable final User user = getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

    @NotNull
    @Override
    public UserShowAllResponse showAll(
            @NotNull final UserShowAllRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final List<User> users = getUserService().findAll();
        return new UserShowAllResponse(users);
    }

    @NotNull
    @Override
    public UserSetAllResponse setAll(
            @NotNull final UserSetAllRequest request
    ) throws AbstractException {
        check(request);
        if (request.getUsers() == null) request.setUsers(Collections.emptyList());
        @Nullable final List<User> users = (List<User>) getUserService().set(request.getUsers());
        return new UserSetAllResponse(users);
    }

}
