package ru.t1.aayakovlev.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.User;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class UserSetAllRequest extends AbstractUserRequest {

    @Nullable
    private List<User> users;


}
