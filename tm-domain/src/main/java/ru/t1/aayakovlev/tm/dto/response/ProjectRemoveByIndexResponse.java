package ru.t1.aayakovlev.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.Project;

public final class ProjectRemoveByIndexResponse extends AbstractProjectResponse {

    public ProjectRemoveByIndexResponse(@Nullable Project project) {
        super(project);
    }

}
