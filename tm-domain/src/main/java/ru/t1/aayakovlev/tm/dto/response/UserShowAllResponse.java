package ru.t1.aayakovlev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.User;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class UserShowAllResponse extends AbstractUserResponse {

    public UserShowAllResponse(@Nullable final List<User> users) {
        super(users);
    }

}
