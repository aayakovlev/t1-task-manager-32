package ru.t1.aayakovlev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.User;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private User user;

    @Nullable
    private List<User> users;

    public AbstractUserResponse(@Nullable final User user) {
        this.user = user;
    }

    public AbstractUserResponse(@Nullable final List<User> users) {
        this.users = users;
    }

}
