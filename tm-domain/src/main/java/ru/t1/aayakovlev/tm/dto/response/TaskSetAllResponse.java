package ru.t1.aayakovlev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskSetAllResponse extends AbstractTaskResponse {

    public TaskSetAllResponse(@Nullable final List<Task> tasks) {
        super(tasks);
    }

}
