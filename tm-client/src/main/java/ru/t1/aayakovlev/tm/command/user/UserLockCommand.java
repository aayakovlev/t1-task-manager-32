package ru.t1.aayakovlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.UserLockRequest;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Lock user by login.";

    @NotNull
    public static final String NAME = "user-lock";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOCK]");
        System.out.print("Enter login: ");
        @NotNull final String login = nextLine();

        @NotNull final UserLockRequest request = new UserLockRequest();
        request.setLogin(login);

        getUserEndpointClient().lock(request);
    }

}
