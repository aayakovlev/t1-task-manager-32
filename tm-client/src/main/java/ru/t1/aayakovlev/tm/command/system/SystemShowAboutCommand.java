package ru.t1.aayakovlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.ServerAboutRequest;
import ru.t1.aayakovlev.tm.dto.response.ServerAboutResponse;

public final class SystemShowAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    public static final String NAME = "about";

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        @NotNull final ServerAboutResponse response = getSystemEndpointClient().getAbout(request);
        System.out.println("[ABOUT]");
        System.out.println("E-mail: " + response.getEmail());
        System.out.println("Author: " + response.getName());
        System.out.println();
        System.out.println("[GIT]");
        System.out.println("Branch: " + response.getGitBranch());
        System.out.println("Commit id: " + response.getGitCommitId());
        System.out.println("Committer: " + response.getGitCommitterName());
        System.out.println("E-mail: " + response.getGitCommitterEmail());
        System.out.println("Message: " + response.getGitCommitMessage());
        System.out.println("Time: " + response.getGitCommitTime());
    }

}
