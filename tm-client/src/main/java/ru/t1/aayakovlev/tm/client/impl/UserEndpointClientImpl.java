package ru.t1.aayakovlev.tm.client.impl;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.client.AuthEndpointClient;
import ru.t1.aayakovlev.tm.client.UserEndpointClient;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;

@NoArgsConstructor
public final class UserEndpointClientImpl extends AbstractEndpointClient implements UserEndpointClient {

    public UserEndpointClientImpl(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authClient = new AuthEndpointClientImpl();
        authClient.connect();
        System.out.println(authClient.login(new UserLoginRequest("test", "test")));
        System.out.println(authClient.profile(new UserShowProfileRequest()).getUser().getEmail());

        @NotNull final UserEndpointClient userClient = new UserEndpointClientImpl((AbstractEndpointClient) authClient);
        System.out.println(userClient.update(new UserUpdateProfileRequest("Test FN", "Test LN", "Test MN")));
        System.out.println(authClient.profile(new UserShowProfileRequest()).getUser().getFirstName());
        System.out.println(authClient.profile(new UserShowProfileRequest()).getUser().getLastName());
        System.out.println(authClient.profile(new UserShowProfileRequest()).getUser().getMiddleName());

        System.out.println(authClient.logout(new UserLogoutRequest()));
        authClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserChangePasswordResponse changePassword(
            @NotNull final UserChangePasswordRequest request
    ) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse lock(
            @NotNull final UserLockRequest request
    ) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRegistryResponse registry(
            @NotNull final UserRegistryRequest request
    ) {
        return call(request, UserRegistryResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRemoveResponse remove(
            @NotNull final UserRemoveRequest request
    ) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserShowAllResponse showAll(
            @NotNull final UserShowAllRequest request
    ) {
        return call(request, UserShowAllResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserSetAllResponse setAll(
            @NotNull final UserSetAllRequest request
    ) {
        return call(request, UserSetAllResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse unlock(
            @NotNull final UserUnlockRequest request
    ) {
        return call(request, UserUnlockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateProfileResponse update(
            @NotNull final UserUpdateProfileRequest request
    ) {
        return call(request, UserUpdateProfileResponse.class);
    }

}
