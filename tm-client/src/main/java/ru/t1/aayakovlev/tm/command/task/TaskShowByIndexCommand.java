package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.request.TaskShowByIndexRequest;
import ru.t1.aayakovlev.tm.dto.response.TaskShowByIndexResponse;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Task;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Show task by index.";

    public static final String NAME = "task-show-by-index";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.print("Enter index: ");
        @NotNull final Integer index = nextNumber() - 1;

        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest();
        request.setIndex(index);
        @Nullable final TaskShowByIndexResponse response = getTaskEndpointClient().showByIndex(request);
        @Nullable final Task task = response.getTask();

        showTask(task);
    }

}
