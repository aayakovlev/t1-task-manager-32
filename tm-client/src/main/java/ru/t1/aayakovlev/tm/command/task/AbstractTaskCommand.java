package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.client.ProjectEndpointClient;
import ru.t1.aayakovlev.tm.client.TaskEndpointClient;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ProjectEndpointClient getProjectEndpointClient() {
        return serviceLocator.getProjectEndpointClient();
    }

    @NotNull
    protected TaskEndpointClient getTaskEndpointClient() {
        return serviceLocator.getTaskEndpointClient();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTasks(@NotNull final List<Task> tasks) {
        @NotNull final AtomicInteger index = new AtomicInteger(1);
        tasks.stream()
                .filter(Objects::nonNull)
                .forEachOrdered((t) -> System.out.println(index.getAndIncrement() + ". " + t));
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Project id: " + task.getProjectId());
        System.out.println("Status: " + Status.toName(task.getStatus()));
    }

}
