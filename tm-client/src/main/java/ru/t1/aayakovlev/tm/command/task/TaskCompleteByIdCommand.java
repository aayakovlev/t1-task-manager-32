package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.TaskCompleteByIdRequest;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Task complete by id.";

    @NotNull
    public static final String NAME = "task-complete-by-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();

        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest();
        request.setId(id);
        request.setStatus(Status.COMPLETED);

        getTaskEndpointClient().completeById(request);
    }

}
