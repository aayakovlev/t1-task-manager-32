package ru.t1.aayakovlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.client.AuthEndpointClient;
import ru.t1.aayakovlev.tm.client.UserEndpointClient;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected UserEndpointClient getUserEndpointClient() {
        return serviceLocator.getUserEndpointClient();
    }

    @NotNull
    protected AuthEndpointClient getAuthEndpointClient() {
        return serviceLocator.getAuthEndpointClient();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final User user) throws EntityNotFoundException {
        if (user == null) throw new EntityNotFoundException();
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Email: " + user.getEmail());
    }

}
