package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.TaskRemoveByIndexRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Task remove by index.";

    @NotNull
    public static final String NAME = "task-remove-by-index";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.print("Enter index: ");
        @NotNull final Integer index = nextNumber() - 1;

        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest();
        request.setIndex(index);

        getTaskEndpointClient().removeByIndex(request);
    }

}
