package ru.t1.aayakovlev.tm.client.impl;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.client.SystemEndpointClient;
import ru.t1.aayakovlev.tm.dto.request.ServerAboutRequest;
import ru.t1.aayakovlev.tm.dto.request.ServerVersionRequest;
import ru.t1.aayakovlev.tm.dto.response.ServerAboutResponse;
import ru.t1.aayakovlev.tm.dto.response.ServerVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClientImpl extends AbstractEndpointClient implements SystemEndpointClient {

    public SystemEndpointClientImpl(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient systemClient = new SystemEndpointClientImpl();
        systemClient.connect();

        System.out.println(systemClient.getVersion(new ServerVersionRequest()).getVersion());

        systemClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(
            @NotNull final ServerAboutRequest request
    ) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(
            @NotNull final ServerVersionRequest request
    ) {
        return call(request, ServerVersionResponse.class);
    }

}
