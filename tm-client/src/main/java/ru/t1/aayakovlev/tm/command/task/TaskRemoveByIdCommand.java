package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.TaskRemoveByIdRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Task remove by id.";

    public static final String NAME = "task-remove-by-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();

        @NotNull TaskRemoveByIdRequest request = new TaskRemoveByIdRequest();
        request.setId(id);

        getTaskEndpointClient().removeById(request);
    }

}
