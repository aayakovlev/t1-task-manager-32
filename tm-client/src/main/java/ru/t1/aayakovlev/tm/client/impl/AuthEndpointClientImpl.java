package ru.t1.aayakovlev.tm.client.impl;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.client.AuthEndpointClient;
import ru.t1.aayakovlev.tm.dto.request.UserLoginRequest;
import ru.t1.aayakovlev.tm.dto.request.UserLogoutRequest;
import ru.t1.aayakovlev.tm.dto.request.UserShowProfileRequest;
import ru.t1.aayakovlev.tm.dto.response.UserLoginResponse;
import ru.t1.aayakovlev.tm.dto.response.UserLogoutResponse;
import ru.t1.aayakovlev.tm.dto.response.UserShowProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClientImpl extends AbstractEndpointClient implements AuthEndpointClient {

    public AuthEndpointClientImpl(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authClient = new AuthEndpointClientImpl();
        authClient.connect();
        System.out.println(authClient.profile(new UserShowProfileRequest()).getUser());
        System.out.println(authClient.login(new UserLoginRequest("admon", "admon")).isSuccess());
        System.out.println(authClient.login(new UserLoginRequest("admin", "admin")).isSuccess());
        System.out.println(authClient.profile(new UserShowProfileRequest()).getUser().getEmail());
        System.out.println(authClient.logout(new UserLogoutRequest()));
        System.out.println(authClient.profile(new UserShowProfileRequest()).getUser());
        authClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(
            @NotNull final UserLoginRequest request
    ) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(
            @NotNull final UserLogoutRequest request
    ) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserShowProfileResponse profile(
            @NotNull final UserShowProfileRequest request
    ) {
        return call(request, UserShowProfileResponse.class);
    }

}
