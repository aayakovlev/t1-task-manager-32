package ru.t1.aayakovlev.tm.command.server;

import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.enumerated.Role;

public abstract class AbstractServerCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
