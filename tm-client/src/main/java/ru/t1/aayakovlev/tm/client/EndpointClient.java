package ru.t1.aayakovlev.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.Socket;

public interface EndpointClient {

    @Nullable
    Socket connect() throws IOException;

    @Nullable
    Socket disconnect() throws IOException;

    void setSocket(@NotNull final Socket socket);

}
