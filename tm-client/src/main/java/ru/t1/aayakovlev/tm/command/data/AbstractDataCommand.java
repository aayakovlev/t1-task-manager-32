package ru.t1.aayakovlev.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.dto.Domain;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.ProjectShowAllResponse;
import ru.t1.aayakovlev.tm.dto.response.TaskShowAllResponse;
import ru.t1.aayakovlev.tm.dto.response.UserShowAllResponse;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import java.util.Collections;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BACKUP = "./backup.json";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BIN = "./data.bin";

    @NotNull
    public static final String FILE_XML = "./data.xml";

    @NotNull
    public static final String FILE_JSON = "./data.json";

    @NotNull
    public static final String FILE_YAML = "./data.yaml";

    @NotNull
    public static final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    public static final String APPLICATION_TYPE_JSON = "application/json";

    @NotNull
    public Domain getDomain() throws AbstractException {
        @NotNull final Domain domain = new Domain();

        @NotNull final UserShowAllRequest requestUser = new UserShowAllRequest();
        @Nullable final UserShowAllResponse responseUser = serviceLocator.getUserEndpointClient().showAll(requestUser);
        if (responseUser.getUsers() == null) responseUser.setUsers(Collections.emptyList());
        domain.setUsers(responseUser.getUsers());

        @NotNull final ProjectShowAllRequest requestProject = new ProjectShowAllRequest();
        @Nullable final ProjectShowAllResponse responseProject = serviceLocator.getProjectEndpointClient().showAll(requestProject);
        if (responseProject.getProjects() == null) responseProject.setProjects(Collections.emptyList());
        domain.setProjects(responseProject.getProjects());

        @NotNull final TaskShowAllRequest requestTask = new TaskShowAllRequest();
        @Nullable final TaskShowAllResponse responseTask = serviceLocator.getTaskEndpointClient().showAll(requestTask);
        if (responseTask.getTasks() == null) responseTask.setTasks(Collections.emptyList());
        domain.setTasks(responseTask.getTasks());

        return domain;
    }

    public void setDomain(@Nullable final Domain domain) throws AbstractException {
        if (domain == null) return;
        @NotNull final UserSetAllRequest requestUser = new UserSetAllRequest(domain.getUsers());
        serviceLocator.getUserEndpointClient().setAll(requestUser);
        @NotNull final ProjectSetAllRequest requestProject = new ProjectSetAllRequest(domain.getProjects());
        serviceLocator.getProjectEndpointClient().setAll(requestProject);
        @NotNull final TaskSetAllRequest requestTask = new TaskSetAllRequest(domain.getTasks());
        serviceLocator.getTaskEndpointClient().setAll(requestTask);
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest();
        serviceLocator.getAuthEndpointClient().logout(requestLogout);
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

}
