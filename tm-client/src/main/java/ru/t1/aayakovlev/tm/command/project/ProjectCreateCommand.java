package ru.t1.aayakovlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.ProjectCreateRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Create new project.";

    @NotNull
    public static final String NAME = "project-create";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter name: ");
        @NotNull final String name = nextLine();
        System.out.print("Enter description: ");
        @NotNull final String description = nextLine();

        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest();
        request.setName(name);
        request.setDescription(description);

        getProjectEndpoint().create(request);
    }

}
