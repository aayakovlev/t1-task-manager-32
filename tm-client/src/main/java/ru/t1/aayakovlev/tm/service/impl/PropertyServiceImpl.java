package ru.t1.aayakovlev.tm.service.impl;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.service.PropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public final class PropertyServiceImpl implements PropertyService {

    @NotNull
    private static final String APPLICATION_FILE_NAME_KEY = "config";

    @NotNull
    private static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String GIT_BRANCH_KEY = "gitBranch";

    @NotNull
    private static final String GIT_COMMIT_ID_KEY = "gitCommitId";

    @NotNull
    private static final String GIT_COMMITTER_NAME_KEY = "gitCommitterName";

    @NotNull
    private static final String GIT_COMMITTER_EMAIL_KEY = "gitCommitterEmail";

    @NotNull
    private static final String GIT_COMMIT_MESSAGE_KEY = "gitCommitMessage";

    @NotNull
    private static final String GIT_COMMIT_TIME_KEY = "gitCommitTime";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "10340";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyServiceImpl() {
        final boolean existsExternalConfig = isExistsExternalConfig();
        if (existsExternalConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @Override
    @NotNull
    public String getGitBranch() {
        return read(GIT_BRANCH_KEY);
    }

    @Override
    @NotNull
    public String getGitCommitId() {
        return read(GIT_COMMIT_ID_KEY);
    }

    @Override
    @NotNull
    public String getGitCommitterName() {
        return read(GIT_COMMITTER_NAME_KEY);
    }

    @Override
    @NotNull
    public String getGitCommitterEmail() {
        return read(GIT_COMMITTER_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE_KEY);
    }

    @Override
    @NotNull
    public String getGitCommitTime() {
        return read(GIT_COMMIT_TIME_KEY);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getIntegerValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File config = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(config);
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

}
