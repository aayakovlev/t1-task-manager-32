package ru.t1.aayakovlev.tm.command.server;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public final class DisconnectCommand extends AbstractServerCommand {

    @NotNull
    public static final String DESCRIPTION = "Disconnect from server";

    @NotNull
    public static final String NAME = "disconnect";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {

    }

}
