package ru.t1.aayakovlev.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.model.Command;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.ServiceLocator;

public abstract class AbstractCommand implements Command {

    @NotNull
    protected ServiceLocator serviceLocator;

    @NotNull
    protected PropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public ServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        @NotNull String result = "";
        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

}
