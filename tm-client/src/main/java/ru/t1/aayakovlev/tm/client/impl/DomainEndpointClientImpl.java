package ru.t1.aayakovlev.tm.client.impl;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.client.DomainEndpointClient;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;

@NoArgsConstructor
public final class DomainEndpointClientImpl extends AbstractEndpointClient implements DomainEndpointClient {

    public DomainEndpointClientImpl(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupLoadResponse backupLoad(
            @NotNull final DataBackupLoadRequest request
    ) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupSaveResponse backupSave(
            @NotNull final DataBackupSaveRequest request
    ) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64LoadResponse base64Load(
            @NotNull final DataBase64LoadRequest request
    ) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64SaveResponse base64Save(
            @NotNull final DataBase64SaveRequest request
    ) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinaryLoadResponse binaryLoad(
            @NotNull final DataBinaryLoadRequest request
    ) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinarySaveResponse binarySave(
            @NotNull final DataBinarySaveRequest request
    ) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonLoadFasterXmlResponse jsonLoadFXml(
            @NotNull final DataJsonLoadFasterXmlRequest request
    ) {
        return call(request, DataJsonLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonLoadJaxBResponse jsonLoadJaxB(
            @NotNull final DataJsonLoadJaxBRequest request
    ) {
        return call(request, DataJsonLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonSaveFasterXmlResponse jsonSaveFXml(
            @NotNull final DataJsonSaveFasterXmlRequest request
    ) {
        return call(request, DataJsonSaveFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonSaveJaxBResponse jsonSaveJaxB(
            @NotNull final DataJsonSaveJaxBRequest request
    ) {
        return call(request, DataJsonSaveJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlLoadFasterXmlResponse xmlLoadFXml(
            @NotNull final DataXmlLoadFasterXmlRequest request
    ) {
        return call(request, DataXmlLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlLoadJaxBResponse xmlLoadJaxB(
            @NotNull final DataXmlLoadJaxBRequest request
    ) {
        return call(request, DataXmlLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlSaveFasterXmlResponse xmlSaveFXml(
            @NotNull final DataXmlSaveFasterXmlRequest request
    ) {
        return call(request, DataXmlSaveFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlSaveJaxBResponse xmlSaveJaxB(
            @NotNull final DataXmlSaveJaxBRequest request
    ) {
        return call(request, DataXmlSaveJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlLoadFasterXmlResponse yamlLoad(
            @NotNull final DataYamlLoadFasterXmlRequest request
    ) {
        return call(request, DataYamlLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlSaveFasterXmlResponse yamlSave(
            @NotNull final DataYamlSaveFasterXmlRequest request
    ) {
        return call(request, DataYamlSaveFasterXmlResponse.class);
    }

}
