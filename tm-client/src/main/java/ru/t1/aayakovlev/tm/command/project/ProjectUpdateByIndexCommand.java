package ru.t1.aayakovlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.ProjectUpdateByIndexRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Update project by index.";

    @NotNull
    public static final String NAME = "project-update-by-index";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        @NotNull final Integer index = nextNumber() - 1;
        System.out.print("Enter new name: ");
        @NotNull final String name = nextLine();
        System.out.print("Enter new description: ");
        @NotNull final String description = nextLine();

        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest();
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);

        getProjectEndpoint().updateByIndex(request);
    }

}
