package ru.t1.aayakovlev.tm.client.impl;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.client.AuthEndpointClient;
import ru.t1.aayakovlev.tm.client.TaskEndpointClient;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;

@NoArgsConstructor
public final class TaskEndpointClientImpl extends AbstractEndpointClient implements TaskEndpointClient {

    public TaskEndpointClientImpl(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authClient = new AuthEndpointClientImpl();
        authClient.connect();
        System.out.println(authClient.login(new UserLoginRequest("test", "test")));
        System.out.println(authClient.profile(new UserShowProfileRequest()).getUser().getEmail());

        @NotNull final TaskEndpointClient taskClient = new TaskEndpointClientImpl((AbstractEndpointClient) authClient);
        System.out.println(taskClient.create(new TaskCreateRequest("test task", "test description")));
        System.out.println(taskClient.showAll(new TaskShowAllRequest()).getTasks());

        System.out.println(authClient.logout(new UserLogoutRequest()));
        authClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse bindToProject(
            @NotNull final TaskBindToProjectRequest request
    ) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIdResponse changeStatusById(
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskClearResponse clear(
            @NotNull final TaskClearRequest request
    ) {
        return call(request, TaskClearResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCompleteByIdResponse completeById(
            @NotNull final TaskCompleteByIdRequest request
    ) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCompleteByIndexResponse completeByIndex(
            @NotNull final TaskCompleteByIndexRequest request
    ) {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCreateResponse create(
            @NotNull final TaskCreateRequest request
    ) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIdResponse removeById(
            @NotNull final TaskRemoveByIdRequest request
    ) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIndexResponse removeByIndex(
            @NotNull final TaskRemoveByIndexRequest request
    ) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskSetAllResponse setAll(
            @NotNull final TaskSetAllRequest request
    ) {
        return call(request, TaskSetAllResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowAllResponse showAll(
            @NotNull final TaskShowAllRequest request
    ) {
        return call(request, TaskShowAllResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIdResponse showById(
            @NotNull final TaskShowByIdRequest request
    ) {
        return call(request, TaskShowByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIndexResponse showByIndex(
            @NotNull final TaskShowByIndexRequest request
    ) {
        return call(request, TaskShowByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByProjectIdResponse showByProjectId(
            @NotNull final TaskShowByProjectIdRequest request
    ) {
        return call(request, TaskShowByProjectIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskStartByIdResponse startById(
            @NotNull final TaskStartByIdRequest request
    ) {
        return call(request, TaskStartByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskStartByIndexResponse startByIndex(
            @NotNull final TaskStartByIndexRequest request
    ) {
        return call(request, TaskStartByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindFromProject(
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIdResponse updateById(
            @NotNull final TaskUpdateByIdRequest request
    ) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIndexResponse updateByIndex(
            @NotNull final TaskUpdateByIndexRequest request
    ) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

}
