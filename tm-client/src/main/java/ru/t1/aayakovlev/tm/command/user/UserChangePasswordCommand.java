package ru.t1.aayakovlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.UserChangePasswordRequest;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Change user's password.";

    @NotNull
    public static final String NAME = "user-change-password";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();
        System.out.print("Enter new password: ");
        @NotNull final String password = nextLine();

        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest();
        request.setId(id);
        request.setPassword(password);

        getUserEndpointClient().changePassword(request);
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

}
