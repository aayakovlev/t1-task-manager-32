package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.client.*;

public interface ServiceLocator {

    @NotNull
    AuthEndpointClient getAuthEndpointClient();

    @NotNull
    CommandService getCommandService();

    @NotNull
    EndpointClient getConnectionEndpointClient();

    @NotNull
    DomainEndpointClient getDomainEndpointClient();

    @NotNull
    LoggerService getLoggerService();

    @NotNull
    ProjectEndpointClient getProjectEndpointClient();

    @NotNull
    PropertyService getPropertyService();

    @NotNull
    SystemEndpointClient getSystemEndpointClient();

    @NotNull
    TaskEndpointClient getTaskEndpointClient();

    @NotNull
    UserEndpointClient getUserEndpointClient();

}
