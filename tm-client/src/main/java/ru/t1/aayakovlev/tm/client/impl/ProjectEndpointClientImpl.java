package ru.t1.aayakovlev.tm.client.impl;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.client.AuthEndpointClient;
import ru.t1.aayakovlev.tm.client.ProjectEndpointClient;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;

@NoArgsConstructor
public final class ProjectEndpointClientImpl extends AbstractEndpointClient implements ProjectEndpointClient {

    public ProjectEndpointClientImpl(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authClient = new AuthEndpointClientImpl();
        authClient.connect();
        System.out.println(authClient.login(new UserLoginRequest("test", "test")));
        System.out.println(authClient.profile(new UserShowProfileRequest()).getUser().getEmail());

        @NotNull final ProjectEndpointClient projectClient = new ProjectEndpointClientImpl((AbstractEndpointClient) authClient);
        System.out.println(projectClient.create(new ProjectCreateRequest("test project", "test description")));
        System.out.println(projectClient.showAll(new ProjectShowAllRequest()).getProjects());

        System.out.println(authClient.logout(new UserLogoutRequest()));
        authClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIdResponse changeStatusById(
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectClearResponse clear(
            @NotNull final ProjectClearRequest request
    ) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompeteByIdResponse completeById(
            @NotNull final ProjectCompeteByIdRequest request
    ) {
        return call(request, ProjectCompeteByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompeteByIdResponse completeByIndex(
            @NotNull final ProjectCompleteByIndexRequest request
    ) {
        return call(request, ProjectCompeteByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCreateResponse create(
            @NotNull final ProjectCreateRequest request
    ) {
        return call(request, ProjectCreateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIdResponse removeById(
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIndexResponse removeByIndex(
            @NotNull final ProjectRemoveByIndexRequest request
    ) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectSetAllResponse setAll(
            @NotNull final ProjectSetAllRequest request
    ) {
        return call(request, ProjectSetAllResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowAllResponse showAll(
            @NotNull final ProjectShowAllRequest request
    ) {
        return call(request, ProjectShowAllResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowByIdResponse showById(
            @NotNull final ProjectShowByIdRequest request
    ) {
        return call(request, ProjectShowByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowByIndexResponse showByIndex(
            @NotNull final ProjectShowByIndexRequest request
    ) {
        return call(request, ProjectShowByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIdResponse startById(
            @NotNull final ProjectStartByIdRequest request
    ) {
        return call(request, ProjectStartByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIndexResponse startByIndex(
            @NotNull final ProjectStartByIndexRequest request
    ) {
        return call(request, ProjectStartByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIdResponse updateById(
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIndexResponse updateByIndex(
            @NotNull final ProjectUpdateByIndexRequest request
    ) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

}
