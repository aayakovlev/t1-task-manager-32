# TASK MANAGER

## DEVELOPER

**NAME**: Yakovlev Anton

**E-MAIL**: aayakovlev@t1-consulting.ru

## SOFTWARE

**OS**: Windows 10 LTSC 1809 (build 17763.2628)

**JDK**: OpenJDK 1.8.0_41

**MAVEN**: 3.6.3

## HARDWARE

**CPU**: AMD R9

**RAM**: 32 Gb

**SSD**: NVMe 512 Gb

## APPLICATION PROPERTIES

| OS ENV             | JAVA OPTS             | EXAMPLE VALUE            | MEANING                                  |
|--------------------|-----------------------|--------------------------|------------------------------------------|
| CONFIG             | -Dconfig              | ./application.properties | external application configuration file  |
| PASSWORD_ITERATION | -Dpassword.iteration  | 6543                     | password hash iterations                 |
| PASSWORD_SECRET    | -Dpassword.secret     | salt                     | password hash salt                       |

## BUILD APPLICATION

````shell
mvn clean install
````

## RUN APPLICATION

````shell
java -jar ./task-manager.jar
````
